FROM node:20-alpine3.18

# 設置工作目錄為/app
WORKDIR /app

# 將 package.json 和 package-lock.json 複製到/app目錄
COPY package*.json ./

EXPOSE 3000

# 安裝相依套件
RUN npm install

# 將整個目錄複製到/app目錄
COPY . .
CMD [ "npm", "start" ]

# # 執行 npm run build 命令來編譯React應用程式
# RUN npm run build

# # 使用 Nginx 官方映像來提供React應用程式的靜態文件
# FROM nginx:stable-alpine3.17

# # 複製編譯完成的React應用程式到Nginx的HTML目錄
# COPY --from=0 /app/build /usr/share/nginx/html

# # 指定Nginx容器執行時的命令
# CMD ["nginx", "-g", "daemon off;"]

# EXPOSE 3000